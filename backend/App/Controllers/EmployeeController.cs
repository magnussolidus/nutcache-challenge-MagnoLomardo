using backend.Domain;
using backend.Libs;
using DocumentValidator;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace backend.App.Controllers;

[Route("api/[controller]")]
[ApiController]
[EnableCors()]
public class EmployeeController : ControllerBase
{
    private readonly DataContext _dbContext;
    private readonly IConfiguration _config;
    private readonly ILogger<EmployeeData> _logger;
    private static bool _updatingContext;

    public EmployeeController(DataContext dbContext, IConfiguration config, ILogger<EmployeeData> logger)
    {
        _dbContext = dbContext;
        _config = config;
        _logger = logger;
    }

#region GetData
    
    [HttpGet("GetSingleEmployeeData", Name = "Get Single Employee Data")]
    public async Task<ActionResult<EmployeeData>> Get(string cpf, CancellationToken cancellationToken)
    {
        if (!CpfValidation.Validate(cpf))
            return BadRequest(_config["ApiFeedbackMessages:InvalidCpf"]);

        try
        {
            var desiredWorker = await _dbContext.NutcacheChallengeMagnoLomardoEmployeeDatas.FirstAsync(
                x => x.Cpf == cpf,
                cancellationToken);
            
            _logger.Log(LogLevel.Information, "{GetEmployee}{EmployeeCpf}",_config["ApiFeedbackMessages:GetSingleEmployee"], desiredWorker.Cpf);
            return Ok(desiredWorker);
        }
        catch (InvalidOperationException ioe)
        {
            _logger.Log(LogLevel.Warning, ioe, "{ExceptionMessage}", ioe.Message);
            return BadRequest(_config["ApiFeedbackMessages:EmployeeNotFound"]);
        }
        catch (TaskCanceledException tce)
        {
            _logger.Log(LogLevel.Information, tce, "{TaskCanceled} \n {ExceptionMessage}", _config["ApiFeedbackMessages:TaskCanceled"], tce.Message);
            return StatusCode(408, _config["ApiFeedbackMessages:TaskCanceled"]);
        }
        catch (Exception e)
        {
            _logger.Log(LogLevel.Error, e, "{ExceptionMessage}",e.Message);
            return BadRequest();
        }

    }
    
    [HttpGet ("GetAllEmployees")]
    public async Task<ActionResult<List<EmployeeData>>> Get(CancellationToken cancellationToken)
    {
        return Ok(await _dbContext.NutcacheChallengeMagnoLomardoEmployeeDatas.ToListAsync(cancellationToken: cancellationToken));
    }
#endregion

#region InsertData

    [HttpPost("AddNewEmployee")]
    public async Task<ActionResult<List<EmployeeData>>> AddEmployee(EmployeeData newEmployee, CancellationToken cancellationToken)
    {
        if (!CpfValidation.Validate(newEmployee.Cpf))
            return BadRequest(string.Format("{0} ({1})", _config["ApiFeedbackMessages:InvalidUpdateData"],
                _config["ApiFeedbackMessages:InvalidCpf"]));

        newEmployee.Cpf = CleanCpfString(newEmployee.Cpf);
        
        if(!EmailValidator.IsValidEmail(newEmployee.Email))
            return BadRequest(string.Format("{0} ({1})", _config["ApiFeedbackMessages:InvalidUpdateData"],
                _config["ApiFeedbackMessages:InvalidEmail"]));
        
        try
        {
            var registeredEmployees = await _dbContext.NutcacheChallengeMagnoLomardoEmployeeDatas.ToListAsync(cancellationToken);
            var registeredEmployee = registeredEmployees.Find(
                x => x.Cpf == newEmployee.Cpf);

            if (registeredEmployee != null)
            {
                _logger.Log(LogLevel.Error, "{RegisteredEmployee}", _config["ApiFeedbackMessages:EmployeeAlreadyExists"]);
                return BadRequest(_config["ApiFeedbackMessages:EmployeeAlreadyExists"]);
            }
            
            var employee = new EmployeeData()
            {
                Cpf = newEmployee.Cpf,
                Name = newEmployee.Name,
                Email = newEmployee.Email,
                BirtDateTime = newEmployee.BirtDateTime,
                Gender = newEmployee.Gender,
                Team = newEmployee.Team,
                StartDate = newEmployee.StartDate
            };
            
            _dbContext.NutcacheChallengeMagnoLomardoEmployeeDatas.Add(employee);
            await SaveContextChangesAsync(cancellationToken);

            _logger.Log(LogLevel.Information, "Employee with CPF {EmployeeCpf} was successfully inserted into the DB!", employee.Cpf);
            return Ok($"Employee with CPF {employee.Cpf} was successfully inserted into the DB!");
        }
        catch (TaskCanceledException tce)
        {
            _logger.Log(LogLevel.Information, tce, "{TaskCanceled} \n {ExceptionMessage}", _config["ApiFeedbackMessages:TaskCanceled"], tce.Message);
            return StatusCode(408, _config["ApiFeedbackMessages:TaskCanceled"]);
        }
        catch (Exception e)
        {
            _logger.Log(LogLevel.Error, e, "{ExceptionMessage}",e.Message);
            return BadRequest();
        }

    }
#endregion

#region RemoveData

    [HttpDelete ("RemoveEmployeeById")]
    public async Task<ActionResult<EmployeeData>> RemoveEmployeeById(int id, CancellationToken cancellationToken)
    {
        var desiredEmployee = await _dbContext.NutcacheChallengeMagnoLomardoEmployeeDatas.FindAsync(id);
        
        if (desiredEmployee == null)
        {
            return BadRequest(_config["ApiFeedbackMessages:EmployeeNotFound"]);
        }

        _dbContext.NutcacheChallengeMagnoLomardoEmployeeDatas.Remove(desiredEmployee);
        
        var save = await SaveContextChangesAsync(cancellationToken);
        if (!save)
        {
            return Conflict(_config["ApiFeedbackMessages:DbBusy"]);
        }

        return Ok(desiredEmployee);
    }
    
    [HttpDelete ("RemoveEmployeeByCpf")]
    public async Task<ActionResult<EmployeeData>> RemoveEmployeeByCpf(string cpf, CancellationToken cancellationToken)
    {
        if (!CpfValidation.Validate(cpf))
            return BadRequest(_config["ApiFeedbackMessages:InvalidCpf"]);
        cpf = CleanCpfString(cpf);
        
        try
        {
            var desiredWorker = await _dbContext.NutcacheChallengeMagnoLomardoEmployeeDatas.FirstAsync(
                x => x.Cpf == cpf,
                cancellationToken);
            
            _dbContext.NutcacheChallengeMagnoLomardoEmployeeDatas.Remove(desiredWorker);
            var save = await SaveContextChangesAsync(cancellationToken);
            if (!save)
            {
                return Conflict(_config["ApiFeedbackMessages:DbBusy"]);
            }

            return Ok(desiredWorker);
        }
        catch (InvalidOperationException ioe)
        {
            _logger.Log(LogLevel.Warning, ioe, "{ExceptionMessage}", ioe.Message);
            return BadRequest(_config["ApiFeedbackMessages:EmployeeNotFound"]);
        }
        catch (TaskCanceledException tce)
        {
            _logger.Log(LogLevel.Information, tce, "{TaskCanceled} \n {ExceptionMessage}", _config["ApiFeedbackMessages:TaskCanceled"], tce.Message);
            return StatusCode(408, _config["ApiFeedbackMessages:TaskCanceled"]);
        }
        catch (Exception e)
        {
            _logger.Log(LogLevel.Error, e, "{ExceptionMessage}",e.Message);
            return BadRequest();
        }
    }

#endregion

#region UpdateData

    [HttpPut ("UpdateEmployeeById")]
    public async Task<ActionResult<EmployeeData>> UpdateEmployee(int id, EmployeeData newEmployeeData, CancellationToken cancellationToken)
    {
        var dbEmployee = await _dbContext.NutcacheChallengeMagnoLomardoEmployeeDatas.FindAsync(id);
        
        if (dbEmployee == null)
        {
            _logger.Log(LogLevel.Error, "{EmployeeNotFound}", _config["ApiFeedbackMessages:EmployeeNotFound"]);
            return BadRequest(_config["ApiFeedbackMessages:EmployeeNotFound"]);
        }
        
        if (!CpfValidation.Validate(newEmployeeData.Cpf))
            return BadRequest(string.Format("{0} ({1})", _config["ApiFeedbackMessages:InvalidUpdateData"],
                _config["ApiFeedbackMessages:InvalidCpf"]));
        
        if(!EmailValidator.IsValidEmail(newEmployeeData.Email))
            return BadRequest(string.Format("{0} ({1})", _config["ApiFeedbackMessages:InvalidUpdateData"],
                _config["ApiFeedbackMessages:InvalidEmail"]));

        dbEmployee.Name = newEmployeeData.Name;
        dbEmployee.Cpf = CleanCpfString(newEmployeeData.Cpf);
        dbEmployee.Email = newEmployeeData.Email;
        dbEmployee.Gender = newEmployeeData.Gender;
        dbEmployee.Team = newEmployeeData.Team;
        dbEmployee.BirtDateTime = newEmployeeData.BirtDateTime;
        dbEmployee.StartDate = newEmployeeData.StartDate;

        await SaveContextChangesAsync(cancellationToken);

        _logger.Log(LogLevel.Information, "Employee with ID {EmployeeId} was successfully updated!", dbEmployee.Id);
        return Ok($"Employee with ID {dbEmployee.Id} was successfully updated!");
    }

    [HttpPut("UpdateEmployeeByCpf")]
    public async Task<ActionResult<EmployeeData>> UpdateEmployee(string cpf, EmployeeData newEmployeeData, CancellationToken cancellationToken)
    {
        if (!CpfValidation.Validate(cpf))
            return BadRequest(_config["ApiFeedbackMessages:InvalidCpf"]);

        try
        {
            var dbEmployee = await _dbContext.NutcacheChallengeMagnoLomardoEmployeeDatas.FirstAsync(
                x => x.Cpf == cpf,
                cancellationToken);

  
            if (!CpfValidation.Validate(newEmployeeData.Cpf))
                return BadRequest(string.Format("{0} ({1})", _config["ApiFeedbackMessages:InvalidUpdateData"],
                    _config["ApiFeedbackMessages:InvalidCpf"]));
            
            if(!EmailValidator.IsValidEmail(newEmployeeData.Email))
                return BadRequest(string.Format("{0} ({1})", _config["ApiFeedbackMessages:InvalidUpdateData"],
                    _config["ApiFeedbackMessages:InvalidEmail"]));

            dbEmployee.Name = newEmployeeData.Name;
            dbEmployee.Cpf = CleanCpfString(newEmployeeData.Cpf);
            dbEmployee.Email = newEmployeeData.Email;
            dbEmployee.Gender = newEmployeeData.Gender;
            dbEmployee.Team = newEmployeeData.Team;
            dbEmployee.BirtDateTime = newEmployeeData.BirtDateTime;
            dbEmployee.StartDate = newEmployeeData.StartDate;

            await SaveContextChangesAsync(cancellationToken);
            
            _logger.Log(LogLevel.Information, "Employee with CPF {EmployeeCpf} was successfully updated!", dbEmployee.Cpf);
            return Ok($"Employee with CPF {dbEmployee.Cpf} was successfully updated!");
        
        }
        catch (InvalidOperationException ioe)
        {
            _logger.Log(LogLevel.Warning, ioe, "{ExceptionMessage}", ioe.Message);
            return BadRequest(_config["ApiFeedbackMessages:EmployeeNotFound"]);
        }
        catch (TaskCanceledException tce)
        {
            _logger.Log(LogLevel.Information, tce, "{TaskCanceled} \n {ExceptionMessage}", _config["ApiFeedbackMessages:TaskCanceled"], tce.Message);
            return StatusCode(408, _config["ApiFeedbackMessages:TaskCanceled"]);
        }
        catch (Exception e)
        {
            _logger.Log(LogLevel.Error, e, "{ExceptionMessage}",e.Message);
            return BadRequest();
        }

    }
#endregion
    private async Task<bool> SaveContextChangesAsync(CancellationToken cancellationToken)
    {
        if (_updatingContext)
            return false;

        _updatingContext = true;
        await _dbContext.SaveChangesAsync(cancellationToken);
        _updatingContext = false;
        return true;
    }

    private string CleanCpfString(string cpf)
    {
        return cpf.Trim().Replace(".", "").Replace("-", "").Remove(11);
    }
}