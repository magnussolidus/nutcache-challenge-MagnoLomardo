using backend.Domain.Enums;
using Newtonsoft.Json;
namespace backend.Domain;

public class EmployeeData
{
    [JsonRequired] public int Id { get; set; }
    [JsonRequired] public string Name { get; set; }
    [JsonRequired] public DateTime BirtDateTime { get; set; }
    [JsonRequired] public Gender Gender { get; set; }
    [JsonRequired] public string Email { get; set; }
    [JsonRequired] public string Cpf { get; set; }
    [JsonRequired] public DateTimeOffset StartDate { get; set; }
    public TeamEnum? Team { get; set; }

}