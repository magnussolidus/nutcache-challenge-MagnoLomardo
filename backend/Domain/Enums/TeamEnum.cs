namespace backend.Domain.Enums;

public enum TeamEnum
{
    Mobile,
    Frontend,
    Backend
}