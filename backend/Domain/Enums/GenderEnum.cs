namespace backend.Domain.Enums;

public enum Gender
{
    Male,
    Female,
    Other
}