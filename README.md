# Nutcache Challenge MagnoLomardo

This is a challenge project for a Backend position at [Nutcache](https://www.nutcache.com/).

---

# Technologies Used
 - [Angular CLI](https://angular.io/cli) 
   - v14.2.8
 - [.NET Core](https://dotnet.microsoft.com/en-us/download) SDK 
   - v6.0.110
 - [Node](https://nodejs.org/en/) 
   - v19.0.1
   - Installed using [nvm](https://github.com/nvm-sh/nvm#usage).
   - NPM v8.19.2
 - [MariaDB](https://mariadb.org/)
   - v10.9.3-MariaDB Arch Linux
   - I have created a user with the following credentials:
       - **username:** nutcache
       - **password:** bnV0Y2FjaGVAMjAyMg==
   - Alternatively, you can update this info on the connection string with your username and password.
 
### Nuget Packages

 - [DocsBR Validator](https://www.nuget.org/packages/DocsBRValidator)
   - v1.3.1
 - [Microsoft Entity Framework Core](https://www.nuget.org/packages/Microsoft.EntityFrameworkCore)
   - v6.0.10
   + Also uses the following extra EF Core packages:
     + Core.Design
     + Core.Relational
     + Core.Sqlite
     + Core.SqlServer
     + Core.Tools
 - [Newtonsoft JSON](https://www.nuget.org/packages/Newtonsoft.Json)
   - v13.0.2-beta2
 - [Pomelo Entity Framework](https://www.nuget.org/packages/Pomelo.EntityFrameworkCore.MySql)
   - v6.0.2
 - [Swashbuckle Asp NET Core](https://www.nuget.org/packages/Swashbuckle.AspNetCore)
   - v6.2.3

## Tools Used
- [GitLab](https://gitlab.com/)
- JetBrains [Webstorm](https://www.jetbrains.com/webstorm/)
- JetBrains [Rider](https://www.jetbrains.com/rider/)
- JetBrains [DataGrip](https://www.jetbrains.com/datagrip/)
- [Konsole](https://konsole.kde.org/)

---

# License
This project is under the [GNU GPLv3](https://gitlab.com/magnussolidus/nutcache-challenge-MagnoLomardo/-/blob/main/COPYING.txt) license.

---

# How to run it?

Make sure you have all the technologies listed on the Technologies used section, and a working web browser 
(like [Firefox](https://www.mozilla.org/en-US/firefox/new/) or [Chrome](https://www.google.com.br/chrome/)). 
## Backend

1. First you should [clone](https://gitlab.com/magnussolidus/nutcache-challenge-MagnoLomardo.git) or [download](https://gitlab.com/magnussolidus/nutcache-challenge-MagnoLomardo/-/archive/main/nutcache-challenge-MagnoLomardo-main.zip) this repository.
    >1. If you downloaded the repository, make sure to extract all the files before continue.
2. Open up the solution on any IDE of your choice.
3. Open the `appsettings.json` file.
4. Verify the following items on your connection string:
   1. Double check the server address;
      1. Define the proper port if needed!
   2. Double check the username;
   3. Double check the password;
5. Once you make sure that your connection string is setup properly, open up a **Terminal** window.
   1. If you are using **Visual Studio** IDE, you can open up the `Package Manager Console`
6. Navigate to the folder in which you cloned the repository/extracted the files.
7. Navigate to the `backend` folder.
8. Run the following command to update your database:
   `dotnet ef database update`
9. Build and Run the solution
   1. By default, you can use the *backend* profile.
   2. You might create your own or adjust it accordingly to your needs.
10. Once the project is running, it might open up a browser window with the Swagger documentation of the API. You can do some rough testing of the API by itself here.
   > Make sure to trust the domain and proceed anyway when trying to access the api, otherwise you will get CORS errors when trying to work on it from the frontend! 
> If you have not changed anything, the address will be [https://localhost:7294/swagger/index.html](https://localhost:7294/swagger/index.html).


## Frontend

1. Navigate to the `frontend` folder.
2. Run the following commands:
   1. `npm install`
   2. `ng serve`
3. Once the build finishes, open up your browser on the following page:
   1. [http://localhost:4200/](http://localhost:4200/)
4. Done - you are ready to use the application!

> Alternatively, you can just the Swagger auto-generated documentation as User Interface.
Once the UI is developed the instructions will be placed here.

---

## Working Time

> This is just an stipulation of the amount of time I spent working on this challenge. 
> Since I did not track the time working on it, I can't provide a precise value.

| Activity Related to | Time Invested (hours) |
|---------------------|-----------------------|
| Frontend            | 11                    |
| Backend             | 7                     |
| Documentation       | 2                     |
| Total               | 20                    |


---

# Challenge Instructions

## Challenge Guidelines:

- The solution should be implemented using C#. It’s important to use the best development
practices. Performance is desirable.
- Send your solution to [rhbrasil@nutcache.com](mailto:rhbrasil@nutcache.com)
- The specific knowledge that will be evaluated, when applied, are:
  - Implementation of requirements
  - Object-oriented programming
  - Good coding practices, according to the platform of your choice
  - Unit tests and code coverage
- You should implement all required requirements before starting to develop the desired
requirements.
- The use of third party libraries and APIs is permitted and recommended if these APIs
enable the development to be faster and more efficient. Avoid reinventing the wheel if
there are options (libraries, for example) that can make some of the work easier.

## Challenge Requirements
Nutcache Brazil it’s growing and we need to create a web application to facilitate people
management, to organize our employee information.

Implement an API using .Net Core.
The following needs have been identified:
- [x] Define and implement a people registration API.
  - We identified the need for the following data:
    - Name (text)*
    - Birth Date (date picker)*
    - Gender (dropdown) *
    - Email *
    - CPF *
    - Start Date (MM/YYYY) *
    - Team (dropdown fixed data: Mobile, Frontend, Backend - could be
    nullable)
  - Data with * are required
- [x] Define and implement a listing API for all employees already registered;
- [x] Define and implement an API to delete a previously registered employee;
- [x] Define and implement an API to edit a previously registered employee;

In addition to the API, you should implement a user interface to access the API features. This
interface can be Implemented using ASP.Net MVC,a command-line interface (Console
application) or a Desktop application.

The application should be developed in accordance with the recommendations below:
- [x] Main page with registered employees grid with edit and disable options for each row
- [x] The grid must display Name, Email, Start Date and Team for each employee;
- [x] Popup to register an employee.
- [x] Popup to edit a registered employee.
- [x] Popup to confirm disable employee action.

# Desirable (optional) requirements
- [x] In addition to the code, also should be delivered by a git repository, with the following
name convention: nutcache-challenge-yourFirstAndLastNameWithoutSpaces
  - [x] A document detailing your technical solution and the instructions to run the
  application must be in the README.md of the root folder of your repository.
  - [x] Only the last tag pushed in the delivery date will be considered.
- [x] The Web front-end using Angular, Vue, React or any other framework
- [ ] Implement unit tests for all the features
- [x] Appropriate use of SOLID, ACID and design patterns (use when necessary), according to
the platform of your choice

---

# Known Issues

There is a typo at `BirtDateTime`