import {Component, OnInit} from '@angular/core';
import {EmployeeData, Gender, TeamEnum} from "../../Models/EmployeeData";
import {EmployeeApiService} from "../../services/employee-api.service";
import {catchError, finalize, tap, throwError} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {MatDialog} from "@angular/material/dialog";
import {EmployeePopUpComponent} from "../employee-pop-up/employee-pop-up.component";


@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})

export class EmployeeListComponent implements OnInit {
  private readonly ROOT_URL = environment.baseUrl + '/Employee';

  displayedColumns: string[] = ['name', 'email', 'startDate', 'team', 'actions'];
  employeesData: EmployeeData[] = [];
  loading: boolean = false;

  constructor(private employeeCallService: EmployeeApiService,
              private httpClient: HttpClient,
              private dialogRef: MatDialog) { }

  ngOnInit(): void {
    this.loadEmployeesData();
  }

  loadEmployeesData() {
    this.loading = true;
    // setTimeout(() => console.log('waiting api  request'), 1000);

    this.employeeCallService.getEmployeesData()
      .pipe(
        tap(data => this.employeesData = data),
        catchError(err => {
          console.error("Error loading the employees data!", err);
          alert("Error loading the employees data!");
          return throwError(err);
        }),
        finalize(() => this.loading = false)
      )
      .subscribe();
  }

  public displayGenderData(input: Gender): string
  {
    return Gender[input];
  }

  public displayTeamData(input: TeamEnum): string
  {
    return TeamEnum[input];
  }

  public async disableEmployee(id: number) {
     await this.deleteEmployeeById(id);
  }

  private async deleteEmployeeById(targetId: number) {

    return this.httpClient.delete(
      this.ROOT_URL + `/RemoveEmployeeById?id=${targetId}`)
      .subscribe(() => this.loadEmployeesData());
  }

  addEmployeeDialog() {
    this.dialogRef.open(EmployeePopUpComponent, {
      width: '80%',
      });
    this.dialogRef.afterAllClosed.subscribe(
      (() => this.loadEmployeesData())
    );
  }

  EditEmployeeDialog(current: EmployeeData)
  {
    this.dialogRef.open(EmployeePopUpComponent, {
      width: '80%',
      data: { selectedEmployee: current }
      });
    this.dialogRef.afterAllClosed.subscribe(
      (() => this.loadEmployeesData())
    );
  }

}
