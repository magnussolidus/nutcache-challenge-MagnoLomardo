import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {EmployeeData, Gender, TeamEnum} from "../../Models/EmployeeData";
import {FormBuilder, Validators} from "@angular/forms";
import {environment} from "../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-employee-pop-up',
  templateUrl: './employee-pop-up.component.html',
  styleUrls: ['./employee-pop-up.component.scss']
})
export class EmployeePopUpComponent implements OnInit {

  hasEmployeeData: boolean = false;
  currentEmployee: EmployeeData;
  form: any;

  private readonly ROOT_URL = environment.baseUrl + '/Employee';

  constructor(public dialogRef: MatDialogRef<EmployeePopUpComponent>,
              @Inject(MAT_DIALOG_DATA)public data: any,
              private fb: FormBuilder,
              private httpClient: HttpClient)
  {
    this.form = this.fb.group({
      name: ['', { validators: [Validators.required, Validators.min(3)]}],
      birthDate: ['', { validators: [Validators.required]}],
      gender: ['', { validators: [Validators.required]}],
      email: ['', { validators: [Validators.required, Validators.email]}],
      cpf: ['', { validators: [Validators.required, Validators.maxLength(11)]}],
      startDate: ['', { validators: [Validators.required]}],
      team: [''],
    });

    if(data) {
      this.currentEmployee = data.selectedEmployee;
      this.hasEmployeeData = true;
    }
    else {
      this.currentEmployee = this.cleanCurrentData();
    }
  }


  ngOnInit(): void {
    if(this.hasEmployeeData)
    {
      this.form = this.fb.group({
        name: [this.currentEmployee.name, { validators: [Validators.required, Validators.min(3)]}],
        birthDate:[this.currentEmployee.birtDateTime, { validators: [Validators.required]}],
        gender: [this.currentEmployee.gender, { validators: [Validators.required]}],
        email: [this.currentEmployee.email, { validators: [Validators.required, Validators.email]}],
        cpf: [this.currentEmployee.cpf, { validators: [Validators.required, Validators.maxLength(11)]}],
        startDate: [this.currentEmployee.startDate, { validators: [Validators.required]}],
        team: [this.currentEmployee.team],
      });
    }
  }

  public closeDialog()
  {
    this.currentEmployee = this.cleanCurrentData();
    this.dialogRef.close();
  }

  private cleanCurrentData(): EmployeeData {
    let cleanUser: EmployeeData;
    cleanUser = {
      id: 0,
      gender: Gender.Male,
      birtDateTime: "Placeholder",
      cpf: "Placeholder",
      email: "Placeholder",
      name: "Placeholder",
      startDate: "Placeholder",
      team: TeamEnum.Backend,
    };
    return cleanUser;
  }

  public getGenderValues(): string[]
  {
    let result = [];
    for(let g in Gender)
    {
      if(isNaN(Number(g))) {
        result.push(g);
      }
    }

    return result;
  }

  public getTeamValues(): string[]
  {
    let result = [];
    for (let t in TeamEnum)
    {
      if(isNaN(Number(t))) {
        result.push(t);
      }
    }

    return result;
  }

  editUserInfo()
  {
    let newData: EmployeeData = {
      id: 0,
      team: this.form.controls.team.value,
      gender: this.form.controls.gender.value,
      startDate: this.form.controls.startDate.value,
      cpf: this.form.controls.cpf.value,
      birtDateTime: this.form.controls.birthDate.value,
      name: this.form.controls.name.value,
      email: this.form.controls.email.value
    };

    this.hasEmployeeData
      ? this.updateEmployeeData(newData, this.currentEmployee.id)
      : this.insertEmployeeData(newData);
  }

  private updateEmployeeData(newData: EmployeeData, id: number)
  {
    console.log(this.form.controls)
    if(!this.form.valid)
    {
      Object.keys(this.form.controls).forEach(campo => {
        const formControl = this.form.get(campo);
        formControl.markAsDirty();
        // formControl.err
      });
      return;
    }

    return this.httpClient.put(
      this.ROOT_URL + `/UpdateEmployeeById?id=${id}`,
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        }),
        id: newData.id,
        name: newData.name,
        birtDateTime: newData.birtDateTime,
        gender: newData.gender,
        email: newData.email,
        cpf: newData.cpf,
        startDate: newData.startDate,
        team: newData.team ?? null,
      })
      .subscribe((s) => console.log(s));
  }

  private insertEmployeeData(newData: EmployeeData) {

    return this.httpClient.post(
      this.ROOT_URL + `/AddNewEmployee`,
      {
        headers: new HttpHeaders({
          "Accept": "text/plain",
          'Content-Type': 'application/json'
        }),
        id: 0,
        name: newData.name,
        birtDateTime: newData.birtDateTime,
        gender: newData.gender,
        email: newData.email,
        cpf: newData.cpf,
        startDate: newData.startDate,
        team: newData.team ?? null,
      })
      .subscribe((s) => console.log(s));
  }

}
