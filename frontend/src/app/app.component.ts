import { Component } from '@angular/core';
import {environment} from "../environments/environment";
import { Title } from "@angular/platform-browser";
import {EmployeePopUpComponent} from "./employee-pop-up/employee-pop-up.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = environment.title;

  ngOnInit() {
    this.titleService.setTitle(this.title);
  }

  constructor(private titleService:Title) {}


}
