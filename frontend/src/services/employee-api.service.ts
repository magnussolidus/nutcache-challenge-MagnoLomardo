import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {EmployeeData} from "../Models/EmployeeData";
import {environment} from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class EmployeeApiService {

  private readonly ROOT_URL = environment.baseUrl + '/Employee';

  constructor(private httpClient: HttpClient) { }

  getEmployeesData() {
    return this.httpClient.get<EmployeeData[]>(this.ROOT_URL + '/GetAllEmployees',
      {

      });
  }

  addNewEmployee(newData: EmployeeData) {

  }

  updateEmployeeData(newData: EmployeeData, id: number) {

  }

  async deleteEmployeeById(targetId: number) {

    let result = this.httpClient.delete(
      this.ROOT_URL + `/RemoveEmployeeById?id=${targetId}`)
      .subscribe((s) => console.log(s));
    console.warn(result);
    return result;
  }

  async deleteEmployeeByCpf(targetCpf: string) {

    return this.httpClient.delete(
      this.ROOT_URL + `RemoveEmployeeByCpf?cpf=${targetCpf}`)
      .subscribe((s) => {
        console.log(s);
      });
  }
}
