export interface EmployeeData {
  id: number;
  name: string;
  birtDateTime: string;
  gender: Gender;
  email: string;
  cpf: string;
  startDate: string;
  team: TeamEnum;
}

export enum Gender {
  Male,
  Female,
  Other,
}

export enum TeamEnum {
  Mobile,
  Frontend,
  Backend
}
